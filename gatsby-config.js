require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    image: `hero.png`,
    title: `Digitopia`,
    description: `We Are Passionate To Make Moroccan’s Life Better, To Give Them Easy Access To Information, And To Provide Them With Platforms To Interact And Strengthen The Community.`,
    author: `@abdullahMoamen`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    `gatsby-plugin-styled-components`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-gatsby-cloud`,
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Gatsby Starter Blog`,
        short_name: `GatsbyJS`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/assets/images/logo.svg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    {
      resolve: `gatsby-source-airtable`,
      options: {
        apiKey: process.env.AIRTABLE_API_KEY || "key24bYK0QkqSBTIE",
        concurrency: 5,
        tables: [
          {
            baseId: process.env.AIRTABLE_BASE || "apptN9gu7fhmGvwsi",
            tableName: `hero`,
            mapping: { image: `fileNode` },
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-strapi`,
      options: {
        apiURL: "http://35.180.119.249",
        
        // "http://localhost:1337",
        queryLimit: 1000,
        collectionTypes: [
          `digitopia-job`,
          `blog`,
          `team`,
          `career`,
          `backend-job-desc`,
          `Ui-Ux-job-desc`,
        ],
      },
    },
  ],
}

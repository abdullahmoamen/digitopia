import React from "react"
import { StaticImage } from "gatsby-plugin-image"

const services = [
  {
    id: 1,
    icon: (
      <StaticImage
        src="../assets/images/icons/iconOne.png"
        placeholder="blurred"
        layout="fixed"
        alt="icon one"
        className="service-icon"
      />
    ),
    title: "For Individual",
    text: `we strive to bring the best of tools to your fingertips. These tools will improve your life quality, give you easy access to information, and help you take the best decision.`,
  },
  {
    id: 2,
    icon: (
      <StaticImage
        src="../assets/images/icons/iconTwo.png"
        placeholder="blurred"
        layout="fixed"
        alt="icon two"
        className="service-icon"
      />
    ),
    title: "For community",
    text: `We know that working as a group can be cumbersome. But also big changes can happen through the community . For that we provide platforms that will boost interactivity & collaboration.`,
  },
  {
    id: 3,
    icon: (
      <StaticImage
        src="../assets/images/icons/iconThree.png"
        placeholder="blurred"
        layout="fixed"
        alt="icon three"
        className="service-icon"
      />
    ),
    title: "For business and professional",
    text: `we are revolutionizing the relationship between you and your clients. Your marketing strategies will be simpler, more effective and less costly.`,
  },
  // {
  //   id: 4,
  //   icon: <BiDevices className="service-icon" />,
  //   title: "Full stack",
  //   text: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam architecto minus, in aut veniam soluta quaerat.`,
  // },
  // {
  //   id: 5,
  //   icon: <SiFlutter className="service-icon" />,
  //   title: "Flutter",
  //   text: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam architecto minus, in aut veniam soluta quaerat.`,
  // },
  // {
  //   id: 6,
  //   icon: <BiMobileVibration className="service-icon" />,
  //   title: "Mobile Apps",
  //   text: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam architecto minus, in aut veniam soluta quaerat.`,
  // },
]

export default services

import React from "react"
import ReactMarkdown from "react-markdown"

const StyledMarkdown = ({ content }) => {
  return <ReactMarkdown source={content} />
}

export default StyledMarkdown

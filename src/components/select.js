import React from "react"
import Select from "react-select"
// import { noOptionsMessageCSS } from "react-select/src/components/Menu"

// const languages = [
//   { value: "HTML5" },
//   { value: "CSS3" },
//   { value: "BOOTSTRAP" },
//   { value: "My Sql" },
//   { value: "graphQl" },
//   { value: "javaScript" },
//   { value: "React.js" },
//   { value: "gatsby" },
// ]
const options = [
  { value: "HTML5", label: "HTML5" },
  { value: "CSS3", label: "CSS3" },
  { value: "BOOTSTRAP", label: "BOOTSTRAP" },
  { value: "My Sql", label: "My Sql" },
  { value: "javaScript", label: "javaScript" },
  { value: "React.js", label: "React.js" },
  { value: "graphQl", label: "graphQl" },
  { value: "gatsby", label: "gatsby" },
]
export default function Selector() {
  return (
    <Select
      defaultValue={[options[1], options[2]]}
      isMulti
      name="colors"
      options={options}
      className="basic-multi-select"
      classNamePrefix="select"
    />
  )
}

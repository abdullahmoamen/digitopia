import React from "react"
import { StaticImage } from "gatsby-plugin-image"
import { Link } from "gatsby"
import Title from "./title"
import { motion } from "framer-motion"

const containerVariants = {
  hidden: {
    opacity: 0,
    x: "100vw",
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: { type: "spring", delay: 1.5 },
  },
  exit: {
    x: "-100vh",
    transition: { ease: "easeInOut" },
  },
}

const containerVariantsTwo = {
  hidden: {
    opacity: 0,
    x: "-100vw",
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: { type: "spring", delay: 2 },
  },
  exit: {
    x: "-100vh",
    transition: { ease: "easeInOut" },
  },
}

function WhoAreWe() {
  return (
    <main className="page">
      <motion.section
        className="contact-page"
        variants={containerVariants}
        initial="hidden"
        animate="visible"
        exit="exit"
      >
        <article className="contact-info">
          <Title title="who we are" />
          <p>
            It all started as a passion. A passion to make Moroccan’s life
            better, to give them easy access to information, to provide them
            with platforms to interact and strengthen the community.All our
            projects are our own creative ideas.we are specializing at creating
            web and mobile applications and contribute to improve people's life
            quality.
          </p>
          <span className="article-span">
            Our motto is better life, strong community....
          </span>
          <Link className="btn who-btn" to="/aboutUs">
            read more
          </Link>
        </article>
        <article className="article2">
          <section className="who-we-are">
            <StaticImage
              src="../assets/images/who.png"
              alt="lab-top"
              className="img"
              placeholder="blurred"
              layout="fullWidth"
            />
          </section>
        </article>
      </motion.section>

      <motion.section
        className="contact-page"
        variants={containerVariantsTwo}
        initial="hidden"
        animate="visible"
        exit="exit"
      >
        <article className="article2">
          <section className="who-we-are">
            <StaticImage
              src="../assets/images/lab.png"
              alt="lab-top"
              className="img"
              placeholder="blurred"
              layout="fullWidth"
            />
          </section>
        </article>
        <article className="contact-info">
          <Title title="our mission" />
          <p>
            Our main goals are to deliver authentic, creative ideas that will
            improve Moroccans’ life, to strengthen the community by providing
            easy to use platforms for collaborating and interacting and to
            contribute to educating the population fora wider adoption of
            digitalization.
          </p>
          <span className="article-span">
            Digitopia is all for better life, strong community...
          </span>
          <Link className="btn who-btn" to="/aboutUs">
            read more
          </Link>
        </article>
      </motion.section>
    </main>
  )
}

export default WhoAreWe

import * as React from "react"
import PropTypes from "prop-types"
// import { useStaticQuery, graphql } from "gatsby";
import "../assets/css/home.css"
import "normalize.css"
import Header from "./header"
import Footer from './footer'

const Layout = ({ children }) => {
  return (
    <>
      <Header siteTitle={`Digitopia`} />
      <main>{children}</main>
      <Footer />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout

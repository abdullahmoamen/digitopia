import React, { useState } from "react"
import { Link } from "gatsby"
import "../assets/css/home.css"
import "normalize.css"
import { IoSearch } from "react-icons/io5"
import { FaAlignJustify } from "react-icons/fa"
import { StaticImage } from "gatsby-plugin-image"
import { motion } from "framer-motion"

const Header = () => {
  const [show, setShow] = useState(false)

  return (
    <>
      <nav className="navbar">
        <div className="nav-center">
          <div className="nav-header">
            <motion.div
              initial={{ x: -150 }}
              animate={{ x: 0 }}
              transition={{ delay: 0.6, type: "spring", stiffness: 220 }}
            >
              <Link to="/">
                <StaticImage
                  src="../assets/images/logo.png"
                  alt="logo"
                  className="nav-header img"
                  placeholder="tracedSVG"
                  style={{ maxWidth: "190px" }}
                />
              </Link>
            </motion.div>
            <button className="nav-btn" onClick={() => setShow(!show)}>
              <FaAlignJustify />
            </button>
          </div>
          <div className={show ? "nav-links show-links" : "nav-links"}>
            <Link
              to="/"
              className="nav-link"
              activeClassName="active-link"
              onClick={() => setShow(false)}
            >
              Home
            </Link>
            <Link
              to="/career"
              className="nav-link"
              activeClassName="active-link"
              onClick={() => setShow(false)}
            >
              Careers
            </Link>
            <Link
              to="/aboutUs"
              className="nav-link"
              activeClassName="active-link"
              onClick={() => setShow(false)}
            >
              about
            </Link>
            <Link
              to="/allBlogs"
              className="nav-link"
              activeClassName="active-link"
              onClick={() => setShow(false)}
            >
              blogs
            </Link>
            <div className="search">
              <input
                type="text"
                className="search__input"
                placeholder="Search here"
              ></input>
              <IoSearch className="search__icon" />
            </div>
            <div className="nav-link ">
              <Link to="/contact" className="btn ">
                Contact
              </Link>
            </div>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Header

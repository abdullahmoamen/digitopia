import React from "react"
import { Formik, Form, Field, ErrorMessage } from "formik"
import "./css/contact.css"

function FormContact() {
  const encode = data => {
    return Object.keys(data)
      .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
      .join("&")
  }
  return (
    <Formik
      initialValues={{
        name: "",
        email: "",
        message: "",
      }}
      onSubmit={(values, actions) => {
        fetch("/", {
          method: "POST",
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          body: encode({ "form-name": "contact-demo", ...values }),
        })
          .then(() => {
            alert("send Successfully !")
            actions.resetForm()
          })
          .catch(() => {
            alert("Error")
          })
          .finally(() => actions.setSubmitting(false))
      }}
      validate={values => {
        const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
        const errors = {}
        if (!values.name) {
          errors.name = "Name Required"
        }
        if (!values.email || !emailRegex.test(values.email)) {
          errors.email = "Valid Email Required"
        }
        if (!values.message) {
          errors.message = "Message Required"
        }
        return errors
      }}
    >
      {() => (
        <Form
          name="contact"
          data-netlify="true"
          method="POST"
          className="form-row"
        >
          <input type="hidden" name="form-name" value="contact" />
          <label htmlFor="name">Name:</label>
          <Field name="name" />
          <ErrorMessage name="name" className="errorMsg" />

          <label htmlFor="email">Email:</label>
          <Field name="email" />
          <ErrorMessage name="email" />

          <label htmlFor="message">Message: </label>
          <Field name="message" component="textarea" />
          <ErrorMessage name="message" />

          <button type="submit" className="btn">
            Submit
          </button>
        </Form>
      )}
    </Formik>
  )
}

export default FormContact

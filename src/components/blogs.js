import React from "react"
import Title from "./title"
import { Link } from "gatsby"
import Blog from "./blog"
// import { StaticImage } from "gatsby-plugin-image"
import "./css/aboutCards.css"
import { graphql, useStaticQuery } from "gatsby"
import Image from "gatsby-image"

function Blogs({ blogs, title }) {
  const data = useStaticQuery(query)

  const {
    allStrapiBlog: { nodes: lastBlog },
  } = data
  return (
    <>
      <Title title={title} />
      {lastBlog.map(lastBlog => {
        return (
          <section className="section-center blog-page">
            <Image
              fluid={lastBlog.images.localFile.childImageSharp.fluid}
              style={{ borderRadius: "7px", minHeight: "25vh" }}
            />
            <article className="article-body">
              <h1 className="last-title">{lastBlog.title}</h1>
              <p>{lastBlog.description}</p>
              <h5>{lastBlog.date}</h5>
              <Link className="btn who-btn" to={`/articles/${lastBlog.id}`}>
                {/* {lastBlog.blog} */}
                Read More
              </Link>
            </article>
          </section>
        )
      })}

      {/* <Title title="blogs" /> */}
      <div className="container blog-container">
        {blogs.map(blog => {
          return (
            <>
              <Blog key={blog.id} {...blog} />
            </>
          )
        })}
      </div>
    </>
  )
}

export const query = graphql`
  {
    allStrapiBlog(filter: { strapiId: { eq: 11 } }) {
      nodes {
        author
        date(formatString: "MMM,Do,Y")
        full_desc
        id
        job_title
        title
        blog
        description
        sub_title
        images {
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`

export default Blogs

import React from "react"
import Image from "gatsby-image"
import { Link } from "gatsby"
import "./css/aboutCards.css"
// import "../pages/styles/hover.css"
import { motion } from "framer-motion"

const buttonVariants = {
  hover: {
    scale: 1.04,
    transition: {
      duration: 0.4,
      yoyo: 10,
    },
  },
}

function Blog({ id, title, images, date, description ,sub_title}) {
  return (
    <>
      <div className="card blog-card">
        <div className="card-header">
          <Image
            fluid={images.localFile.childImageSharp.fluid}
            className="blog-img"
          />
        </div>
        <div className="card-body blog-body">
          <div style={{ marginLeft: "-0.5rem" }}>
            <Link className="link" to={`/articles/${id}`}>
              {title}
            </Link>
            <h6 style={{margin:'0.3rem 0.6rem',opacity:'0.8'}}>{sub_title}</h6>

          </div>
          <p className="blog-date">{date}</p>
          <h6 style={{ margin: "0", fontWeight: "400" }}>{description}</h6>
        </div>
        <motion.div
          variants={buttonVariants}
          whileHover="hover"
          className="link"
        >
          <Link className="link" to={`/articles/${id}`}>
            {" "}
            Read More{" "}
          </Link>
        </motion.div>
      </div>
    </>
  )
}

export default Blog

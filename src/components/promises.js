import React from "react"
import Title from "./title"
import { StaticImage } from "gatsby-plugin-image"
import "./css/promises.css"

function Promises() {
  return (
    <section className="bg-grey">
      <Title title="our promises" />
      <div className="section-center services-center">
        <article className="services promises">
          <StaticImage
            className="promises-img"
            src="../assets/images/impr.svg"
            placeholder="blurred"
            alt="provide"
            layout="fullWidth"
          />
          <h3>improved life quality</h3>
        </article>
        <article className="services promises">
          <StaticImage
            className="promises-img"
            src="../assets/images/rev.svg"
            placeholder="blurred"
            alt="revolutionizing ideas"
            layout="fullWidth"
          />
          <h3>revolutionizing ideas</h3>
        </article>
        <article className="services promises">
          <StaticImage
            className="promises-img"
            src="../assets/images/str.svg"
            placeholder="blurred"
            alt="stronger community"
            layout="fullWidth"
          />
          <h3>stronger community</h3>
        </article>
      </div>
    </section>
  )
}

export default Promises

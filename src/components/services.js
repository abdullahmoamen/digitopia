import React from "react"
import Title from "./title"
import services from "../constants/services"
import "./css/services.css"

const Services = () => {
  return (
    <>
      <section className="section bg-grey">
        <Title title="our services" />
        <div className="section-center services-center">
          {services.map(service => {
            const { id, icon, title, text } = service
            return (
              <article key={id} className="service">
                {icon}
                <h3>{title}</h3>
                <p>{text}</p>
              </article>
            )
          })}
        </div>
      </section>
    </>
  )
}

export default Services

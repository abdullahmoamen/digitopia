import React, { useState, useEffect } from "react"
import Background from "./background"
import styled from "styled-components"
import { Link } from "gatsby"
import { FiChevronRight, FiChevronLeft } from "react-icons/fi"
import { motion } from "framer-motion"

const buttonVariants = {
  hover: {
    scale: 1.1,
    transition: {
      duration: 0.4,
      yoyo: 10,
    },
  },
}

const Hero = ({ hero }) => {
  const images = hero.map(item => {
    const {
      data: {
        image: { localFiles },
      },
    } = item
    const image = localFiles[0].childImageSharp.fluid
    return image
  })
  const [index, setIndex] = useState(0)
  useEffect(() => {
    const lastIndex = images.length - 1
    if (index < 0) {
      setIndex(lastIndex)
    }
    if (index > lastIndex) {
      setIndex(0)
    }
  }, [index, images])
  return (
    <Wrapper>
      <Background image={images[index]}>
        <motion.article
          initial={{ y: -250 }}
          animate={{ y: 20 }}
          transition={{ delay: 0.4, type: "spring", stiffness: 220 }}
        >
          <h1>better life, strong community</h1>
          <h3>
            We are passionate to make Moroccan’s life better, to give them easy
            access to information, and to provide them with platforms to
            interact and strengthen the community.
          </h3>
          <motion.div variants={buttonVariants} whileHover="hover">
            <Link to="/contact" className="btn">
              contact
            </Link>
          </motion.div>
        </motion.article>

        <button className="prev-btn" onClick={() => setIndex(index - 1)}>
          <FiChevronLeft />
        </button>
        <button className="next-btn" onClick={() => setIndex(index + 1)}>
          <FiChevronRight />
        </button>
        <div className="dots">
          {images.map((_, btnIndex) => {
            return (
              <button
                key={btnIndex}
                onClick={() => setIndex(btnIndex)}
                className={index === btnIndex ? "active" : undefined}
              >
                .
              </button>
            )
          })}
        </div>
      </Background>
    </Wrapper>
  )
}

const Wrapper = styled.section`
  article {
    width: 85vw;
    max-width: 800px;
    color: var(--primary-400);
    text-align: center;
    h1 {
      text-transform: capitalize;
      font-weight: 600;
      font-size: 3rem;
      line-height: 1.25;
      margin: -6rem 0 -2rem 0;
    }
    h3 {
      font-weight: 400;
      color: #040404;
      margin: 4rem;
    }
    /* a {
      border: 2px solid var(--clr-white);
      text-transform: capitalize;
      color: var(--primary-0);
      font-size: 1rem;
      cursor: pointer;
      transition: var(--transition);
    } */
    a:hover {
    }
    @media (min-width: 800px) {
      /* padding: 0 1rem; */
      a {
        font-size: 1rem;
        padding: 0.6rem 2rem;
      }
      h1 {
        letter-spacing: 5px;
      }
    }
  }
  .next-btn,
  .prev-btn {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    background: transparent;
    color: white;
    cursor: pointer;
    transition: var(--transition);
    @media (min-width: 800px) {
      & {
        font-size: 2.5rem;
      }
    }
  }
  .next-btn:hover,
  .prev-btn:hover {
    color: var(--primary-500);
  }
  .prev-btn {
    left: 0;
  }
  .next-btn {
    right: 0;
  }

  @media (min-width: 1000px) {
    .prev-btn {
      left: 3rem;
    }
    .next-btn {
      right: 3rem;
    }
  }

  .dots {
    position: absolute;
    bottom: 5%;
    left: 50%;
    transform: translateX(-50%);
    display: flex;
    justify-content: center;
    button {
      display: block;
      cursor: pointer;
      height: 0.5rem;
      width: 0.5rem;
      border-radius: 50%;
      background: transparent;
      color: transparent;
      margin: 0 0.5rem;
      border: 2px solid #fff;
      padding: 0.2rem;
      @media (min-width: 800px) {
        /* & {
          height: 1rem;
          width: 1rem;
        } */
      }
    }
    button.active {
      background-color: var(--primary-500);
    }
  }
`

export default Hero

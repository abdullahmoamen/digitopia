import React from "react"
import { StaticImage } from "gatsby-plugin-image"
import { Link } from "gatsby"
import { FiLinkedin, FiTwitter, FiYoutube, FiFacebook } from "react-icons/fi"
import "./css/footer.css"

const Footer = () => {
  return (
    <>
      <footer className="footer">
        <div className="footer-container">
          <div className="footer-row">
            <div className="footer-col zero">
              <StaticImage
                src="../assets/images/white-logo.svg"
                className="footer-img"
                layout="fullWidth"
                placeholder="blurred"
                alt="footer pic"
              />
            </div>
            <div className="footer-col one">
              <h4>company</h4>
              <ul>
                <li>
                  <Link to="/aboutUs">about us</Link>
                </li>
                <li>
                  <Link to="/">our projects</Link>
                </li>
                <li>
                  <Link to="/">privacy policy</Link>
                </li>
                <li>
                  <Link to="/">affiliate programs</Link>
                </li>
                <li></li>
              </ul>
            </div>
            <div className="footer-col two">
              <h4>products</h4>
              <ul>
                <li>
                  <Link to="/">product1</Link>
                </li>
                <li>
                  <Link to="/">product2</Link>
                </li>
                <li>
                  <Link to="/"> product3</Link>
                </li>
                <li>
                  <Link to="/">product4</Link>
                </li>
              </ul>
            </div>
            <div className="footer-col three">
              <h4>resources</h4>
              <ul>
                <li>
                  <Link to="/">News</Link>
                </li>
                <li>
                  <Link to="/">Blog</Link>
                </li>
                <li>
                  <Link to="/">Videos</Link>
                </li>
                <li>
                  <Link to="/">FAQs</Link>
                </li>
              </ul>
            </div>
            <div className="footer-col four">
              <h4>Follow us</h4>
              <ul className="footer-social-links">
                <Link to="https://www.linkedin.com/company/digitopia-studio-ltd">
                  <FiLinkedin />
                </Link>
                <Link to="/">
                  <FiTwitter />
                </Link>
                <Link to="/">
                  <FiYoutube />
                </Link>
                <Link to="https://www.facebook.com/digitopiastudio">
                  <FiFacebook />
                </Link>
              </ul>
              <input
                type="text"
                placeholder="@your email"
                className="footer-input"
              />
              <Link to="/" className="footer-btn btn">
                Subscribe
              </Link>
            </div>
            <div className="footer-col five">
              <ul>
                <li>Products</li>
                <li>services</li>
                <li>resources</li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
      <div className="footer-bottom">
        <StaticImage
          className="footer-copyright"
          src="../assets/images/copyright.png"
          alt="copyright"
          layout="fullWidth"
          placeholder="blurred"
        />
      </div>
    </>
  )
}

export default Footer

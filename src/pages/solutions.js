import React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"

function Solutions() {
  return (
    <Layout>
      <Seo title="solutions" />
      <div>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Expedita
        obcaecati dicta laudantium sunt fugiat eum natus ut dignissimos eos
        voluptate, hic, ratione eveniet placeat esse! Aut obcaecati deleniti
        amet doloribus laudantium, iusto quo corrupti reprehenderit similique at
        molestias repellendus impedit, provident fuga harum expedita rem quos
        alias esse illum architecto. Libero unde minus recusandae optio,
        temporibus eos voluptas autem veniam reiciendis fugit assumenda
        dignissimos, corrupti facere deserunt quod laudantium dolorem?
      </div>
    </Layout>
  )
}

export default Solutions

import React from "react"
import Layout from "../components/layout"
import FormContact from "../components/contactForm"
import Seo from "../components/seo"
function ContactForm() {
  return (
    <Layout>
      <Seo title="Contact" />
      <main className="page">
        <section className="contact-page">
          <article className="contact-info">
            <h3>Want To Get In Touch?</h3>
            <p>
              We’d Love to Hear From You Whether you’re curious about features,
              a free trial, or even press—we’re ready to answer any and all
              questions.
            </p>
            {/* <p>Cardigan prism bicycle rights put a bird on it deep v.</p> */}
            <p>
              Let’s get this conversation started. Tell us a bit about yourself,
              and we’ll get in touch as soon as we can..
            </p>
          </article>
          <FormContact className="form-row" />
        </section>
      </main>
    </Layout>
  )
}

export default ContactForm

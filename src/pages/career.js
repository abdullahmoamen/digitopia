import React from "react"
import Title from "../components/title"
import Layout from "../components/layout"
import "./styles/career.css"
import { graphql, useStaticQuery, Link } from "gatsby"
import Image from "gatsby-image"
import Seo from "../components/seo"

function Career() {
  const data = useStaticQuery(query)
  const {
    allStrapiCareer: { nodes: career },
  } = data
  return (
    <Layout>
      <Seo title="Careers" />
      <div className="career-title" style={{marginBottom: '5rem'}}>
        <Title title="Avaliable positions" />
      </div>
      <div className="container career-container">
        {career.map(item => {
          return (
            <div className="card" key={item.id}>
              <div className="icon">
                <Image
                  fluid={item.icon.localFile.childImageSharp.fluid}
                  alt={item.title}
                  key={item.id}
                />
              </div>
              <div className="title">{item.title}</div>
              <div className="features">
                <p>{item.description}</p>
                <footer className="under">
                  <span className="time">{item.cat}</span>
                  <span className="level">{item.level}</span>
                </footer>
              </div>
              <div className="links">
                <Link to={item.desc_link} className="read">
                  read more
                </Link>

                <Link
                  to={item.link}
                  target="_blank"
                  className="btn"
                  style={{ color: "white" }}
                >
                  apply now
                </Link>
              </div>
            </div>
          )
        })}
      </div>
    </Layout>
  )
}

const query = graphql`
  {
    allStrapiCareer {
      nodes {
        cat
        icon {
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        level
        link
        title
        desc_link
        description
        id
      }
    }
  }
`

export default Career

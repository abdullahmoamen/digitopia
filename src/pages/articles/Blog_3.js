import React from "react"
import Layout from "../../components/layout"
import { StaticImage } from "gatsby-plugin-image"
import "../styles/articles.css"
import Seo from "../../components/seo"

function Articles() {
  return (
    <Layout>
      <Seo title="better life" />
      <main className="articles">
        <header className="hero">
          <StaticImage
            src="../../assets/images/pexels-energepiccom-159888.jpg"
            alt="Articles"
            className="hero-img"
            placeholder="tracedSVG"
            layout="fullWidth"
          />
          <div className="hero-container">
            <div className="hero-text">
              <h1>Analyzing</h1>
              <h3>Digitopia For better Life</h3>
            </div>
            <div className="right-content">
              <div className="author">
                 
                <span>Youssef Jarid</span>
              </div>
              <h5>Founder of Digitopia studio</h5>
              <h6>30 June</h6>
            </div>
          </div>
        </header>
      </main>
      <div className="page">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Impediteveniet officiis nisi, obcaecati nesciunt porro quas
          repellat,blanditiis tenetur iusto deserunt! Possimus ducimus, illo ea
          itaquetenetur delectus repudiandae saepe reiciendis expedita harum
          aliquidbeatae distinctio accusamus quo assumenda temporibus, corrupti
          iuremaiores voluptates porro! Cum nostrum cupiditate illum beatae quod
          vel?Natus nemo itaque ipsam dicta delectus, atque nisi inventore
          molestiaeporro nesciunt nobis quaerat qui repellat quibusdam mollitia?
          Commodi nam dolor tempora excepturi facilis. Nisi ipsa eum ipsum
          inventore!Distinctio nemo magni et atque corporis, tempore omnis
          dignissimos!
        </p>
        <h3>Easy access to information</h3>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. A dolores
          laborum minus deserunt fugit ipsum numquam eligendi praesentium animi
          veniam.
        </p>
        <h3>accessability to every one</h3>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit
          porro corporis quia excepturi enim adipisci, facilis, dolor expedita
          quis distinctio ipsum ullam aspernatur harum illum veniam amet,
          laborum odio atque modi maiores cupiditate et. Ex.
        </p>
        <h3>Digitalizing well</h3>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit
          porro corporis quia excepturi enim adipisci, facilis, dolor expedita
          quis distinctio ipsum ullam aspernatur harum illum veniam amet,
          laborum odio atque modi maiores cupiditate et. Ex.
        </p>{" "}
        <h3>Designing responsive websites</h3>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit
          porro corporis quia excepturi enim adipisci, facilis, dolor expedita
          quis distinctio ipsum ullam aspernatur harum illum veniam amet,
          laborum odio atque modi maiores cupiditate et. Ex.
        </p>
      </div>
    </Layout>
  )
}

export default Articles

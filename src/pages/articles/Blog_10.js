import React from "react"
import Layout from "../../components/layout"
import "../styles/articles.css"
import { graphql, useStaticQuery } from "gatsby"
import Seo from "../../components/seo"
import Image from "gatsby-image"
import ReactMarkdown from "react-markdown"

function Articles() {
  const data = useStaticQuery(query)
  const {
    allStrapiBlog: { nodes: blog },
  } = data
  return (
    <Layout>
      <Seo title="Digitalization" />
      {blog.map(item => {
        return (
          <>
            <main className="articles" key={item.id}>
              <header className="hero">
                <Image
                  fluid={item.images.localFile.childImageSharp.fluid}
                  alt="item 10 hero image"
                  className="hero-img"
                  placeholder="tracedSVG"
                  layout="fullWidth"
                />
                <div className="hero-container">
                  <div className="hero-text">
                    <h1>{item.title}</h1>
                    <h2 style={{ opacity: 1 , fontWeight: '200' , fontFamily: 'Lato'}}>{item.sub_title}</h2>
                  </div>
                  <div className="right-content">
                    <div className="author">
                      <span>{item.author}</span>
                    </div>
                    <h5>{item.job_title}</h5>
                    <h6>{item.date}</h6>
                  </div>
                </div>
              </header>
            </main>
            <div className="page">
              <ReactMarkdown>{item.full_desc}</ReactMarkdown>
            </div>
          </>
        )
      })}
    </Layout>
  )
}

const query = graphql`
  {
    allStrapiBlog(filter: { strapiId: { eq: 10 } }) {
      nodes {
        author
        date(formatString: "MMM,Do,Y")
        full_desc
        id
        job_title
        title
        sub_title
        images {
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`

export default Articles

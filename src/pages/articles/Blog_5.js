import React from "react"
import Layout from "../../components/layout"
import { StaticImage } from "gatsby-plugin-image"
import "../styles/articles.css"
import Seo from "../../components/seo"

function Articles() {
  return (
    <Layout>
      <Seo title="Last news" />
      <main className="articles">
        <header className="hero">
          <StaticImage
            src="../../assets/images/img5.jpg"
            alt="Articles"
            className="hero-img"
            placeholder="tracedSVG"
            layout="fullWidth"
          />
          <div className="hero-container">
            <div className="hero-text">
              <h1>Aly: Promoting Accountability</h1>
            </div>

            <div className="right-content">
              <div className="author">
                 
                <span>Youssef Jarid</span>
              </div>
              <h5>Founder of Digitopia studio</h5>
              <h6>9 Aug</h6>
            </div>
          </div>
        </header>
      </main>
      <div className="page">
        <h3>
          In the last article we discuss one of the main benefits of Aly: Easy
          Access to Information{" "}
        </h3>
        <span>
          In this article will cover another important benefit: Promoting
          Accountability.
        </span>
        <hr />
        <h3>
          How many times you receive a bad, humiliating service from a business?
        </h3>
        <p>
          Your immediate reaction is to inform everyone and warn them about this
          business, but at the end you just let it go with no action just
          because there is no platform you can trust to share your experience
          with others and make sure your voice is heard. One major factor of bad
          quality of services Moroccans receive from businesses, is that
          businesses are not held accountable they know that an unsatisfied
          customer can’t harm them that much, and same time a satisfied customer
          will not benefit them a lot. And that is true the individual is
          powerless toward businesses.
        </p>
        <hr />
        <p>
          In the last article we discuss that Aly will provide a platform for
          sharing information, with algorithm to analyze data. This very core
          functionality will have another major benefit is promoting
          accountability. Aly is a platform where you can share your story as
          review with everyone. also, we use our algorithm to combine all your
          voices and score the business, so if the business receives many bad
          reviews, it will receive a very low score from our algorithm which
          will penalize it, consequently it will lose clients. At the opposite
          if a business receives good reviews, he will be giving a high score,
          by consequence will benefit from the many rewards Aly provide,
          especially being more visible on our platform, so it will be
          definitely getting more clients.
        </p>{" "}
        <p>
          Aly will revolutionize the relationship between clients and
          businesses. Aly platform will start a new era where businesses need to
          take a major decision of either riding on this wave of change and
          implement strategies making client satisfaction their top priority, or
          they will be left behind.
        </p>
        <span>
          Aly will play a major role at promoting this culture of accountability
          and by consequences improve quality of services we receive from
          businesses.{" "}
        </span>
      </div>
    </Layout>
  )
}

export default Articles

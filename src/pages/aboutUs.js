import React from "react"
import Layout from "../components/layout"
import { graphql, useStaticQuery, Link } from "gatsby"
import Image from "gatsby-image"
import "./styles/aboutUs.css"
import "../pages/styles/career.css"
import { StaticImage } from "gatsby-plugin-image"
import { FiLinkedin, FiTwitter, FiGithub, FiFacebook } from "react-icons/fi"
import { AiOutlineBehance } from "react-icons/ai"
import { CgWebsite } from "react-icons/cg"
import Seo from "../components/seo"

const AboutUs = () => {
  const data = useStaticQuery(query)
  const {
    allStrapiTeam: { nodes: team },
  } = data
  // const { img, description, name, job_title, links, id } = team[0]
  return (
    <Layout>
      <Seo title="About Us" />

      <section className="about-page">
        <StaticImage
          src="../assets/images/digitopia.svg"
          className="about-img"
        />
        <div className="about-center">
          <article className="about-text">
            <h1 className="titles" style={{ marginTop: "4rem" }}>
              Digitopia Studio
            </h1>
            <p>
              It all started as a passion. A passion to make Moroccans’ life
              better, to give them easy access to information, provide platforms
              that support them in their day to day actions. and help them
              interact and strengthen the community. Whether you are an
              individual or a business, our solutions and designs are out to
              digitalize, automate, and revolutionize different aspects of your
              life. All our projects are our own creative ideas. We believe that
              any digital solution should be authentic and tailored to the
              Moroccans’ specific needs. This is why we take our time
              researching and reflecting before executing. We will take the time
              to simplify our solutions and accompany our users in their
              learning process. We have plenty of ideas to improve your life
              quality, and to make our community stronger. this is our promise
              to all Moroccans.
            </p>
          </article>
        </div>
      </section>

      <h1 className="titles" style={{ margin: "1em 0 6rem" }}>
        Our Team
      </h1>
      <div className="container">
        {team.map(item => {
          return (
            <div className="card" key={item.id} style={{ padding: "0px 10px" }}>
              <div className="card-header">
                <Image
                  fluid={item.img.localFile.childImageSharp.fluid}
                  alt={item.name}
                />
              </div>
              <div className="card-body">
                <h3 className="userName">{item.name}</h3>
                <h4 className="jobName">{item.job_title}</h4>
                {item.description ? <p>{item.description}</p> : ""}
                <div className="user">
                  <div className="user-info">
                    <ul className="social">
                      {item.links.linkedIn ? (
                        <Link to={item.links.linkedIn} target="_blank">
                          <li>
                            <FiLinkedin className="fi" />
                          </li>
                        </Link>
                      ) : (
                        ""
                      )}
                      {item.links.twitter ? (
                        <Link to={item.links.twitter} target="_blank">
                          <li>
                            <FiTwitter className="fi" />
                          </li>
                        </Link>
                      ) : (
                        ""
                      )}

                      {item.links.github ? (
                        <Link to={item.links.github} target="_blank">
                          <li>
                            <FiGithub className="fi" />
                          </li>
                        </Link>
                      ) : (
                        ""
                      )}

                      {item.links.facebook ? (
                        <Link to={item.links.facebook} target="_blank">
                          <li>
                            <FiFacebook className="fi" />
                          </li>
                        </Link>
                      ) : (
                        ""
                      )}
                      {item.links.behance ? (
                        <Link to={item.links.behance} target="_blank">
                          <li>
                            <AiOutlineBehance className="fi" />
                          </li>
                        </Link>
                      ) : (
                        ""
                      )}
                      {item.links.website ? (
                        <Link to={item.links.website} target="_blank">
                          <li>
                            <CgWebsite className="fi" />
                          </li>
                        </Link>
                      ) : (
                        ""
                      )}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    </Layout>
  )
}

const query = graphql`
  {
    allStrapiTeam(sort: { fields: description, order: ASC }) {
      nodes {
        id
        name
        job_title
        description
        links {
          facebook
          github
          id
          linkedIn
          twitter
          behance
          website
        }
        img {
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`
export default AboutUs

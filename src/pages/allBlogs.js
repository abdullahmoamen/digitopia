import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Image from "gatsby-image"
import { Link } from "gatsby"
import { motion } from "framer-motion"
import Title from "../components/title"

const buttonVariants = {
  hover: {
    scale: 1.04,
    transition: {
      duration: 0.4,
      yoyo: 10,
    },
  },
}

const IndexPage = ({ data }) => {
  const {
    allStrapiBlog: { nodes: blogs },
  } = data
  return (
    <Layout>
      <Title title={"All Blogs"} style={{ margin: "0" }} />

      {/* <Blogs blogs={blogs} title="All blogs & last news" /> */}
      <div className="container blog-container">
        {blogs.map(blog => {
          return (
            <div className="card blog-card">
              <div className="card-header">
                <Image
                  fluid={blog.images.localFile.childImageSharp.fluid}
                  className="blog-img"
                />
              </div>
              <div className="card-body blog-body">
                <div style={{ marginLeft: "-0.5rem" }}>
                  <Link className="link" to={`/articles/${blog.id}`}>
                    {blog.title}
                  </Link>
                </div>
                <p className="blog-date">{blog.date}</p>
                <h6 style={{ margin: "0", fontWeight: "400" }}>
                  {blog.description}
                </h6>
              </div>
              <motion.div
                variants={buttonVariants}
                whileHover="hover"
                className="link"
              >
                <Link className="link" to={`/articles/${blog.id}`}>
                  {" "}
                  Read More{" "}
                </Link>
              </motion.div>
            </div>
          )
        })}
      </div>
    </Layout>
  )
}

export const query = graphql`
  {
    allStrapiBlog(sort: { fields: date, order: DESC }, limit: 6) {
      nodes {
        slug
        description
        date(formatString: "MMM,Do,Y")
        id
        title
        sub_title
        blog
        images {
          localFile {
            childImageSharp {
              fluid {
                src
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`

export default IndexPage

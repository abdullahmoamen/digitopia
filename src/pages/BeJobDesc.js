import React, { useState } from "react"
import Title from "../components/title"
import Layout from "../components/layout"
import Seo from "../components/seo"
import "./styles/job-description.css"
import { Link, graphql, useStaticQuery } from "gatsby"
import { FaAngleDoubleRight } from "react-icons/fa"
import { motion } from "framer-motion"

const containerVariants = {
  hidden: {
    opacity: 0,
    x: "80vw",
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: { type: "spring", delay: 0.5 },
  },
  exit: {
    x: "-80vh",
    transition: { ease: "easeInOut" },
  },
}

const query = graphql`
  {
    allStrapiBackendJobDesc(sort: { fields: strapiId, order: ASC }) {
      nodes {
        strapiId
        strapiFields
        title
        level
        desc {
          id
          name
        }
      }
    }
  }
`

const JobDescribtion = () => {
  const data = useStaticQuery(query)
  const [value, setValue] = useState(0)

  const {
    allStrapiBackendJobDesc: { nodes: jobs },
  } = data
  const { title, desc, level, link } = jobs[value]

  return (
    <>
      <Layout>
        <Seo title="Backend JobDescription" />
        <section className="section jobs">
          <Title title="Backend JobDescription" />
          <div className="jobs-center">
            <div className="btn-container">
              {jobs.map((item, index) => {
                return (
                  <button
                    onClick={() => setValue(index)}
                    className={`job-btn ${index === value && "active-btn"}`}
                    key={item.strapiId}
                  >
                    {item.strapiFields}
                  </button>
                )
              })}
              <Link to={link} className="btn apply" target="_blank">
                Apply now
              </Link>
            </div>

            <article className="job-info">
              <h3>{title}</h3>
              <br />
              <h4>{level}</h4>
              <h4>Part Time</h4>
              {desc.map(item => {
                return (
                  <motion.div
                    variants={containerVariants}
                    initial="hidden"
                    animate="visible"
                    exit="exit"
                    key={item.id}
                    className="job-desc"
                  >
                    <FaAngleDoubleRight className="job-icon"></FaAngleDoubleRight>
                    <p>{item.name}</p>
                  </motion.div>
                )
              })}
            </article>
          </div>
        </section>
      </Layout>
    </>
  )
}
export default JobDescribtion

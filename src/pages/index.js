import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import WhoAreWe from "../components/who-are-we"
import Blogs from "../components/blogs"
import Hero from "../components/hero"
import { graphql } from "gatsby"
import Services from "../components/services"
import Promises from "../components/promises"

const IndexPage = ({ data }) => {
  const {
    allAirtable: { nodes: hero },
    allStrapiBlog: { nodes: blogs },
  } = data
  return (
    <Layout>
      <Seo title="Home" />
      <Hero hero={hero} />
      <WhoAreWe />
      <Services />
      <Promises />
      <Blogs blogs={blogs} title="blogs & News" showLink />
    </Layout>
  )
}

export const query = graphql`
  {
    allAirtable(
      filter: { table: { eq: "hero" } }
      sort: { fields: data___date, order: ASC } # limit: 3
    ) {
      nodes {
        id
        data {
          date
          name
          type
          image {
            localFiles {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
    allStrapiBlog(sort: { fields: date, order: ASC }, limit: 2) {
      nodes {
        slug
        description
        date(formatString: "MMM,Do,Y")
        id
        title
        sub_title
        blog
        images {
          localFile {
            childImageSharp {
              fluid {
                src
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`

export default IndexPage

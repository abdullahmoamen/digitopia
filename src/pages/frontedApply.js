import React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import "./styles/contact.css"
import Selector from "../components/select"
import { GrReactjs } from "react-icons/gr"
import { Link } from "gatsby"
// import { useForm } from "react-hook-form"
// import { yupResolver } from "@hookform/resolvers/yup"
// import * as yup from "yup"

// const schema = yup.object().shape({
//   name: yup.string().required("name is required").min(3),
//   email: yup.string().email().required("email is required"),
//   msg: yup.string().required("you must provide a message").min(6),
//   //   select: yup.string().required("you must enter your skills !"),
//   file: yup
//     .mixed()
//     .required("you need to provide a file")
//     .test("fileSize", "The file is too large", value => {
//       return value && value[0].size <= 2000000
//     })
//     .test("type", "you must provide a file ex: ex.pdf or ex.docx", value => {
//       return (
//         (value && value[0].type === "application/pdf") ||
//         (value &&
//           value[0].type ===
//             "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
//       )
//     }),
// })
const Contact = () => {
  // const {
  //   register,
  //   handleSubmit,
  //   formState: { errors },
  // } = useForm({
  //   resolver: yupResolver(schema),
  // })
  // const onSubmit = data => alert(JSON.stringify(data))

  return (
    <Layout>
      <Seo title="Contact" />
      <main className="page">
        <section className="contact-page">
          <article className="contact-info">
            <div className="card jobApply">
              <div className="title">Fronted developer</div>
              <div className="icon">
                <GrReactjs />
              </div>

              <div className="features">
                <p>
                  Build clean, responsive and performant user interfaces for
                  challenging applications. Collaborate with different team
                  member including manager, frontend developer, backend
                  developers and UI/UX expert Monitoring and improving
                  application performance. troubleshooting and fixing bugs.
                </p>
                <footer className="under">
                  <span className="time">part time</span>
                  <span className="level">junior level</span>
                </footer>
                <Link to="/FeJobDesc.js" className="read">
                  read more
                </Link>
              </div>
            </div>
          </article>
          <article>
            <form
              className="form contact-form"
              action="https://formspree.io/f/moqywloj"
              method="POST"
              // onSubmit={handleSubmit(onSubmit)}
              // action='POST'
              // data-netlify='true'
            >
              <div className="mb-3">
                <label htmlFor="file">upload your CV</label>
                <input
                  required
                  type="file"
                  name="file"
                  id="file"
                  // {...register("file")}
                  title="select a file"
                />
                {/* <p style={{ color: "red" }}>{errors.file?.message}</p> */}
              </div>
              <div className="form-row">
                <label htmlFor="name">your name</label>
                <input
                  required
                  type="text"
                  name="name"
                  id="name"
                  // {...register("name")}
                />
                {/* <p style={{ color: "red" }}>{errors.name?.message}</p> */}
              </div>
              <div className="form-row">
                <label htmlFor="email">your email</label>
                <input
                  required
                  type="text"
                  name="email"
                  id="email"
                  // {...register("email")}
                />
                {/* <p style={{ color: "red" }}>{errors.email?.message}</p> */}
              </div>

              <div className="form-row">
                <Selector
                // {...register("select")}
                />
                {/* <p style={{ color: "red" }}>{errors.select?.message}</p> */}
              </div>

              <div className="form-row">
                <control>years of experience</control>
                <select as="select" custom style={{ width: "100%" }}>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </select>
              </div>

              <div className="form-row">
                <label htmlFor="message">Tell Us about yourself</label>
                <textarea
                  placeholder="tell us about yourself"
                  name="message"
                  id="message"
                  // {...register("msg")}
                ></textarea>
                {/* <p style={{ color: "red" }}>{errors.msg?.message}</p> */}
              </div>
              <button type="submit" className="btn block">
                submit
              </button>
            </form>
          </article>
        </section>
      </main>
    </Layout>
  )
}

export default Contact
